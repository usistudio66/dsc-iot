package com.ddstudio.dsc.services;

/**
 * Created by nhingu on 5/19/15.
 *
 */
public interface ResponseCallback <T> {

    public void onSuccess(T data);

    public void onError(Exception ex, String errorMessage);
}
