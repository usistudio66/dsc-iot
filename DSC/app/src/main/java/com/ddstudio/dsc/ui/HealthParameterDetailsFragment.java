package com.ddstudio.dsc.ui;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.models.HealthParameter;
import com.ddstudio.dsc.models.HealthParameterDayHistory;
import com.ddstudio.dsc.services.NetworkAPIClient;
import com.ddstudio.dsc.services.ResponseCallback;
import com.ddstudio.dsc.ui.view.CustomTextView;
import com.ddstudio.dsc.utils.DateUtils;
import com.ddstudio.dsc.utils.StringUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by vassharma on 9/23/2015.
 */
public class HealthParameterDetailsFragment extends Fragment {

    public static final String TAG = "HealthParameterDetailsFrag";

    @InjectView(R.id.dateTextView)
    CustomTextView mDateTextView;

    @InjectView(R.id.unitTextView)
    CustomTextView mUnitTextView;

    @InjectView(R.id.bar_chart)
    BarChart mChart;

    @InjectView(R.id.progressBar)
    ProgressBar progressBar;

    private int noOfDays;

    private String memberId, parameterID;

    private ArrayList<BarEntry> mBarEntry;

    private ArrayList<String> mBarEntryLabelsList;

    private BarData mBarData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_health_parameter_details, container, false);
        ButterKnife.inject(this, rootView);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("Days")) {
            noOfDays = bundle.getInt("Days");
            memberId = bundle.getString("MemberId");
            parameterID = bundle.getString("ParameterId");
        }

        mBarEntry = new ArrayList<>();
        mBarEntryLabelsList = new ArrayList<String>();

        if (DSCApplication.DEBUG) {
            getSavedJsonData();
        } else {
            getParameterDetails(noOfDays);
        }
        return rootView;
    }

    private void getSavedJsonData() {
        String jsonOutput = StringUtils.loadJSONFromAsset(getActivity(), "bargraph.json");
        Gson gson = new Gson();
        Type listType = new TypeToken<List<HealthParameter>>() {
        }.getType();
        List<HealthParameter> posts = (List<HealthParameter>) gson.fromJson(jsonOutput, listType);
        showBarGraph(posts);
    }

    public void addValuesToBarEntry(List<HealthParameterDayHistory> data) {

        for (int i = 0; i < data.size(); i++) {
            float value = data.get(i).kpiValue;
            mBarEntry.add(new BarEntry(value, i));
        }
    }

    public void addValuesToBarEntryLabels(List<HealthParameterDayHistory> data) {

        for (int i = 0; i < data.size(); i++) {
            String value = data.get(i).activityDate;
            mBarEntryLabelsList.add(DateUtils.getDateValueFromFullDate(value));
        }
        calculateDateForNoOFDays(data.get(0).activityDate, data.get(data.size() - 1).activityDate);
    }

    private void calculateDateForNoOFDays(String date1, String date2) {//int days

        // SimpleDateFormat sdfDateMonth = new SimpleDateFormat("dd MMM, yy");
        // Calendar cal = Calendar.getInstance();
        // String date1 = sdfDateMonth.format(cal.getTime());
        // cal.roll(Calendar.DAY_OF_YEAR, -days);
        // String date2 = sdfDateMonth.format(cal.getTime());

        mDateTextView.setText(String.format(getActivity().getResources().getString(R.string.date_text_format),
                DateUtils.getChartDisplayDate(date1), DateUtils.getChartDisplayDate(date2)));
    }

    private void getParameterDetails(int noOfDays) {
        progressBar.setVisibility(View.VISIBLE);
        mChart.setVisibility(View.GONE);
        NetworkAPIClient.parameterDetails(memberId, parameterID, noOfDays, new ResponseCallback<List<HealthParameter>>() {
            @Override
            public void onSuccess(List<HealthParameter> datalist) {

                if (datalist == null || datalist.size() == 0) {
                    //Toast.makeText(getActivity(), "No Data Available", Toast.LENGTH_SHORT).show();
                    return;
                }
                showBarGraph(datalist);
            }

            @Override
            public void onError(Exception ex, String errorMessage) {
                Toast.makeText(getActivity(), "No Data Available", Toast.LENGTH_SHORT).show();
                showBarGraph(null);
                progressBar.setVisibility(View.GONE);
                mChart.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(errorMessage))
                    Log.e(TAG, errorMessage);
            }
        });
    }

    private void showBarGraph(List<HealthParameter> datalist) {

        if (datalist != null) {
            mUnitTextView.setText(StringUtils.camelCasingString(datalist.get(0).getUnit()));

            Collections.sort(datalist.get(0).getData(), new ListComparator());

            addValuesToBarEntry(datalist.get(0).getData());
            addValuesToBarEntryLabels(datalist.get(0).getData());
        }

        BarDataSet bardataset = new BarDataSet(mBarEntry, "");
        mBarData = new BarData(mBarEntryLabelsList, bardataset);
        bardataset.setBarSpacePercent(90f);

        bardataset.setDrawValues(false);//Remove values from each bar

        bardataset.setColor(getResources().getColor(R.color.dusty_orange));
        bardataset.setValueTextColor(ContextCompat.getColor(getActivity(), R.color.white));

        mChart.setDescription("");
        mChart.getAxisRight().setEnabled(false);//to get y-axis unit at left side only
        mChart.setMaxVisibleValueCount(60);// if more than 60 entries are displayed in the chart, no values will be drawn
        mChart.setPinchZoom(false);
        mChart.getLegend().setEnabled(false);//Remove color labels from graph
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setTextSize(12);
        xAxis.setDrawGridLines(false);
        mChart.setVisibleXRangeMaximum(6);

        YAxis leftAxis = mChart.getAxisLeft();
        mChart.getXAxis().setLabelsToSkip(0);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setTextSize(12);
        leftAxis.setAxisMinValue(0f);

        mChart.setExtraBottomOffset(20f);
        mChart.setData(mBarData);
        mChart.animateY(1000);
        mChart.setScaleMinima(mBarEntry.size() / 7f, 1f);
        mChart.invalidate();
        progressBar.setVisibility(View.GONE);
        mChart.setVisibility(View.VISIBLE);

    }

    private class ListComparator implements Comparator<HealthParameterDayHistory> {

        @Override
        public int compare(HealthParameterDayHistory hp1, HealthParameterDayHistory hp2) {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz", Locale.ENGLISH);
            Date convertDate = new Date();
            Date convertDate2 = new Date();
            try {
                convertDate = dateFormat.parse(hp1.activityDate);
                convertDate2 = dateFormat.parse(hp2.activityDate);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (convertDate == convertDate2) {
                return 0;
            } else if (convertDate.before(convertDate2)) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
