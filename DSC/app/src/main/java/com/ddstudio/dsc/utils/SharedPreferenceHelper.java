package com.ddstudio.dsc.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.ddstudio.dsc.gcm.GcmHelper;
import com.ddstudio.dsc.models.User;

/**
 * Created by nhingu on 9/14/15.
 */
public class SharedPreferenceHelper {


    public static String getMemberID(Context context) {
        final SharedPreferences prefs = GcmHelper.getGCMPreferences(context);
        String memberId = prefs.getString(Constants.PROPERTY.PROPERTY_USER_ID, "");
//        userName = prefs.getString(Constants.PROPERTY.PROPERTY_USER_NAME, "");

        return memberId;
    }

    public static User getUserDetails(Context context) {
        final SharedPreferences prefs = GcmHelper.getGCMPreferences(context);
        User user = new User();

        user.member_id = prefs.getString(Constants.PROPERTY.PROPERTY_USER_ID, "");
        user.name = prefs.getString(Constants.PROPERTY.PROPERTY_USER_NAME, "");
        user.user_type = prefs.getString(Constants.PROPERTY.PROPERTY_USER_TYPE, "");

        return user;

    }

}
