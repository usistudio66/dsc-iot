package com.ddstudio.dsc.models;

import com.ddstudio.dsc.utils.StringUtils;

/**
 * Created by nhingu on 5/19/15.
 */
public class User {

    public static final String TYPE_MEMBER      = "Member";
    public static final String TYPE_CLINICIAN   = "Clinician";
    public static final String TYPE_PROVIDER    = "Provider";

    public String member_id;
    public String message;
    public String name;
    public String assigned_provider;
    public String assigned_clinician;
    public String communication_preference;
    public String user_type;

    public boolean isClinician() {
        return (StringUtils.isEqualsIgnoreCase(user_type, TYPE_CLINICIAN));
    }

    public boolean isMember() {
        return (StringUtils.isEqualsIgnoreCase(user_type, TYPE_MEMBER));
    }

    public boolean isProvider() {
        return (StringUtils.isEqualsIgnoreCase(user_type, TYPE_PROVIDER));
    }

}
