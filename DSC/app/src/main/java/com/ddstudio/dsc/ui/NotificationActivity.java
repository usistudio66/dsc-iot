package com.ddstudio.dsc.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.models.Notification;
import com.ddstudio.dsc.models.NotificationHistory;
import com.ddstudio.dsc.services.NetworkAPIClient;
import com.ddstudio.dsc.services.ResponseCallback;
import com.ddstudio.dsc.utils.Constants;
import com.ddstudio.dsc.utils.DateUtils;
import com.ddstudio.dsc.utils.StringUtils;
import com.google.gson.GsonBuilder;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class NotificationActivity extends BaseActivity {

    private static final String TAG = "NotificationActivity";

    @InjectView(R.id.shownotification)
    ImageButton mShownotification;

    @InjectView(R.id.refresh)
    ImageButton refresh;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @InjectView(R.id.listView)
    ListView mListView;

    String memberId;
    NotificationHistory history = null;
    LazyAdapter listAdapter;
    @InjectView(R.id.searchMember)
    SearchView searchMember;
    @InjectView(R.id.delete_notification)
    ImageButton deleteNotificationButton;

    public static void showNotificationHistory(Activity activity, String memberId) {
        Intent i = new Intent(activity, NotificationActivity.class);
        i.putExtra(Constants.PARAM.MEMBER_ID, memberId);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.inject(this);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
            mShownotification.setVisibility(View.GONE);

            searchMember.setVisibility(View.GONE);
            deleteNotificationButton.setVisibility(View.VISIBLE);
            this.refresh.setVisibility(View.GONE);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        memberId = getIntent().getExtras().getString(Constants.PARAM.MEMBER_ID);

        listAdapter = new LazyAdapter(this);
        this.mListView.setAdapter(listAdapter);


        if (DSCApplication.DEBUG) {
            getDataFromJson();
        } else {
            getAllMessages();
        }
    }

    private void getDataFromJson() {
        String notificationJson = StringUtils.loadJSONFromAsset(getBaseContext(), "notificationJson.json");
        history = new GsonBuilder().create().fromJson(notificationJson, NotificationHistory.class);
        hideProgressDialog();
        listAdapter.notifyDataSetInvalidated();
    }

    private void getAllMessages() {
        ResponseCallback<NotificationHistory> historyResponseCallback = new ResponseCallback<NotificationHistory>() {
            @Override
            public void onSuccess(NotificationHistory data) {
                history = data;
                hideProgressDialog();
                listAdapter.notifyDataSetInvalidated();
            }

            @Override
            public void onError(Exception ex, String errorMessage) {
                Toast.makeText(getApplicationContext(), "No Data Available", Toast.LENGTH_SHORT).show();
                Log.e(TAG, errorMessage);
                hideProgressDialog();
            }
        };

        NetworkAPIClient.notificationHistory(memberId, historyResponseCallback);
        showProgressDialog();
    }

    public class LazyAdapter extends BaseAdapter {
        private LayoutInflater mInflater = null;

        public LazyAdapter(Activity activity) {
            mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (history == null || history.notifications == null) {
                return 0;
            } else {
                return history.notifications.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return history.notifications.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //Not implementing the ViewHolder Pattern time crunch
            View vi = convertView;

            if (vi == null)
                vi = mInflater.inflate(R.layout.notification_list_item, null);

            TextView message = (TextView) vi.findViewById(R.id.tv_message); // title
            TextView date = (TextView) vi.findViewById(R.id.tv_date); // artist name

            Notification notification = (Notification) getItem(position);

            message.setText(notification.message);
            date.setText(DateUtils.getNotificationDisplayDate(notification.timestamp));

            return vi;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
