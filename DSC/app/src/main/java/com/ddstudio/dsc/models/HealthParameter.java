package com.ddstudio.dsc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nhingu on 5/27/15.
 */
public class HealthParameter implements Serializable {

    @SerializedName("kpi_name")
    @Expose
    public String name;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @SerializedName("kpi_unit")
    @Expose
    public String unit;

    @SerializedName("kpi_critical")
    @Expose
    public String critical;

    @SerializedName("kpi_id")
    @Expose
    public String id;

    @SerializedName("kpi_value")
    @Expose
    public double currentDayValue;

    @SerializedName("kpi_threshold")
    public long threshold;

    @SerializedName("data")
    @Expose
    public List<HealthParameterDayHistory> data = new ArrayList<HealthParameterDayHistory>();

    @SerializedName("kpi_min")
    @Expose
    public double minValue;

    @SerializedName("kpi_max")
    @Expose
    public double maxValue;

    public long getCurrentDayValue() {
        return (int) currentDayValue;
    }

    public long getCurrentDayMinValue() {
        return (int) minValue;
    }

    public long getCurrentDayMAxValue() {
        return (int) maxValue;
    }

    public List<HealthParameterDayHistory> getData() {
        return data;
    }

    public void setData(List<HealthParameterDayHistory> data) {
        this.data = data;
    }
}
