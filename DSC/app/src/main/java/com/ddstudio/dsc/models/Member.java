package com.ddstudio.dsc.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ddstudio.dsc.utils.StringUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nhingu on 9/13/15.
 */
public class Member implements Serializable {

    @SerializedName("member_id")
    @Expose
    private String memberId;

    @SerializedName("member_name")
    @Expose
    private String memberName;

    @SerializedName("member_gender")
    @Expose
    private String memberGender;

    @Expose
    private List<HealthParameter> parameters = new ArrayList<HealthParameter>();


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return (StringUtils.isBlank(memberName)?"":memberName);
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberGender() {
        return memberGender;
    }

    public boolean isMale() {
        return StringUtils.isEqualsIgnoreCase(memberGender, "Male");
    }

    public boolean isFemale() {
        return StringUtils.isEqualsIgnoreCase(memberGender, "Female");
    }

    public void setMemberGender(String memberGender) {
        this.memberGender = memberGender;
    }

    public List<HealthParameter> getParameters() {
        return parameters;
    }

    public boolean hasParameters() {
        return (parameters!=null && parameters.size()>0);
    }

    public void setParameters(List<HealthParameter> parameters) {
        this.parameters = parameters;
    }
}
