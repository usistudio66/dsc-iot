package com.ddstudio.dsc.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nhingu on 5/29/15.
 */
public class HealthParameters {

    public static final String STEPS_ID = "1";
    public static final String HEART_RATE_ID = "2";
    public static final String SODIUM_ID = "3";
    public static final String CHOLESTROL_ID = "4";
    public static final String CALORIE_ID = "5";
    public static final String SLEEP_ID = "6";
    public static final String CARBOHYDRATE_ID = "7";
    public static final String FIBER_ID = "8";

    @Expose
    public List<HealthParameter> parameters = new ArrayList<HealthParameter>();

    @Expose
    public String success;

    @Expose
    public int status;


    public HealthParameter getParameterWithID(String id) {

        HealthParameter parameter = null;

        for(HealthParameter parm : parameters) {
            if(parm.id == id) {
                parameter = parm;
                break;
            }
        }

        return parameter;
    }

}
