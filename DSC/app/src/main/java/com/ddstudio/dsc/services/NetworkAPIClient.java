package com.ddstudio.dsc.services;

import android.app.Notification;

import com.ddstudio.dsc.models.HealthParameter;
import com.ddstudio.dsc.models.HealthParameters;
import com.ddstudio.dsc.models.NotificationHistory;
import com.ddstudio.dsc.models.ProvidersSummaryData;
import com.ddstudio.dsc.models.User;
import com.ddstudio.dsc.utils.Constants;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by nhingu on 5/19/15.
 */
public class NetworkAPIClient {

    private static RestAdapter mRestAdapter;

    private static final String API_ENDPOINT = "http://ec2-34-206-194-116.compute-1.amazonaws.com:8080/spring-rest-service-0.0.1-SNAPSHOT/";

    //"http://10.118.36.91:9999/api/api/";

    private static final String MOCK_API_ENDPOINT = "http://beta.json-generator.com";

    private static final boolean run_mock_services = false;


    public static RestAdapter getRestAdapter() {
        String endpoint = API_ENDPOINT;

        if (run_mock_services) {
            endpoint = MOCK_API_ENDPOINT;
        }

        if (mRestAdapter == null) {
            mRestAdapter = new RestAdapter.Builder()
                    .setEndpoint(endpoint)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
        }
        return mRestAdapter;
    }

    public static void loginUser(String userName, String password, final ResponseCallback<User> listener) {

        Callback<User> userCallback = new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                listener.onSuccess(user);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onError(error, error.getMessage());
            }
        };

        if (run_mock_services) {
            getRestAdapter().create(ILoginService.class).mockLoginUser(userCallback);
        } else {
            HashMap<String, String> loginParams = new HashMap<>(2);
            loginParams.put(Constants.PARAM.USERNAME, userName);
            loginParams.put(Constants.PARAM.PASSWORD, password);
            getRestAdapter().create(ILoginService.class).loginUser(loginParams, userCallback);
        }
    }

    public static void registerDevice(String memberId, String registrationID, String deviceId, final ResponseCallback<Notification> listener) {

        HashMap<String, String> params = new HashMap<>();

        params.put(Constants.PARAM.MEMBER__ID, memberId);
        params.put(Constants.PARAM.REG_ID, registrationID);
        params.put(Constants.PARAM.CREATED_DATE, (new SimpleDateFormat(Constants.DATE_FORMATS.LONG_SERVER_DATE_FORMAT)).format(new Date(System.currentTimeMillis())));
        //params.put(Constants.PARAM.ANDROID_DEVICE_ID, deviceId); //It was added for conditions if user can user multiple devices, but right now limiting it to one

        getRestAdapter().create(ILoginService.class).registerDevice(params, new Callback<Notification>() {
            @Override
            public void success(Notification notification, Response response) {
                listener.onSuccess(notification);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onError(error, error.getMessage());
            }
        });
    }

    /**
     * Service to help for user login
     */
    public interface ILoginService {

        @POST(Constants.URL.LOGIN)
        public void loginUser(@Body HashMap<String, String> body, Callback<User> callback);

        @GET(Constants.MOCK_URL.LOGIN)
        public void mockLoginUser(Callback<User> callback);

        @POST(Constants.URL.REGSITER_DEVICE)
        public void registerDevice(@Body HashMap<String, String> params, Callback<Notification> listener);
    }


    public static void userSummaryData(String memberID, final ResponseCallback<HealthParameters> listener) {

        Callback<HealthParameters> paramCallback = new Callback<HealthParameters>() {
            @Override
            public void success(HealthParameters parameters, Response response) {
                listener.onSuccess(parameters);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onError(error, error.getMessage());
            }
        };

        if (run_mock_services) {
            getRestAdapter().create(ISummaryService.class).mockUserSummary(paramCallback);
        } else {
            getRestAdapter().create(ISummaryService.class).userSummary(memberID, paramCallback);
        }
    }

    public static void providerSummaryData(String memberID, final ResponseCallback<ProvidersSummaryData> listener) {

        Callback<ProvidersSummaryData> paramCallback = new Callback<ProvidersSummaryData>() {
            @Override
            public void success(ProvidersSummaryData parameters, Response response) {
                listener.onSuccess(parameters);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onError(error, error.getMessage());
            }
        };

        if (run_mock_services) {
            getRestAdapter().create(ISummaryService.class).mockProviderSummary(paramCallback);
        } else {
            getRestAdapter().create(ISummaryService.class).providerSummary(memberID, paramCallback);
        }
    }

    /**
     * Service to help for user Summary
     */
    public interface ISummaryService {

        @GET(Constants.URL.PROVIDER_SUMMARY)
        public void providerSummary(@Query(Constants.PARAM.PROVIDER_ID) String memberId, Callback<ProvidersSummaryData> callback);

        @GET(Constants.MOCK_URL.PROVIDER_SUMMARY)
        public void mockProviderSummary(Callback<ProvidersSummaryData> callback);

        @GET(Constants.URL.SUMMARY)
        public void userSummary(@Query(Constants.PARAM.MEMBER_ID) String memberId, Callback<HealthParameters> callback);

        @GET(Constants.MOCK_URL.SUMMARY)
        public void mockUserSummary(Callback<HealthParameters> callback);
    }


    public static void parameterDetails(String memberID, String parameterId, int days, final ResponseCallback<List<HealthParameter>> listener) {

        Callback<List<HealthParameter>> paramCallback = new Callback<List<HealthParameter>>() {
            @Override
            public void success(List<HealthParameter> parameter, Response response) {
                listener.onSuccess(parameter);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onError(error, error.getMessage());
            }
        };

        if (run_mock_services) {
            getRestAdapter().create(IParameterDetailsService.class).mockParameterDetails(paramCallback);
        } else {
            getRestAdapter().create(IParameterDetailsService.class).parameterDetails(memberID, parameterId, days, paramCallback);
        }
    }

    /**
     * Service to help for user login
     */
    public interface IParameterDetailsService {

        @GET(Constants.URL.PARAMETER_DETAILS)
        public void parameterDetails(@Query(Constants.PARAM.MEMBER_ID) String memberId, @Query(Constants.PARAM.PARAMETER_ID) String parameterId, @Query(Constants.PARAM.Days) int days, Callback<List<HealthParameter>> callback);

        @GET(Constants.MOCK_URL.PARAMETER_DETAILS)
        public void mockParameterDetails(Callback<List<HealthParameter>> callback);
    }


    public static void notificationHistory(String memberID, final ResponseCallback<NotificationHistory> listener) {

        Callback<NotificationHistory> notificationCallback = new Callback<NotificationHistory>() {
            @Override
            public void success(NotificationHistory notifications, Response response) {
                listener.onSuccess(notifications);
            }

            @Override
            public void failure(RetrofitError error) {
                listener.onError(error, error.getMessage());
            }
        };

        if (run_mock_services) {
            getRestAdapter().create(INotificationHistoryService.class).mockNotificationHistory(notificationCallback);
        } else {
            getRestAdapter().create(INotificationHistoryService.class).notificationHistory(memberID, notificationCallback);
        }
    }

    /**
     * Service to help for user login
     */
    public interface INotificationHistoryService {
        
        @GET(Constants.URL.NOTIFICATION)
        public void notificationHistory(@Query(Constants.PARAM.MEMBER_ID) String memberId, Callback<NotificationHistory> callback);

        @GET(Constants.MOCK_URL.NOTIFICATION)
        public void mockNotificationHistory(Callback<NotificationHistory> callback);
    }

}
