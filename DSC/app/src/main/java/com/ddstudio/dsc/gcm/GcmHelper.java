package com.ddstudio.dsc.gcm;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ddstudio.dsc.services.NetworkAPIClient;
import com.ddstudio.dsc.services.ResponseCallback;
import com.ddstudio.dsc.utils.Constants;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

public class GcmHelper {

    private String mRegId;
    private Context mContext;
    private String mUserId;

    private static GoogleCloudMessaging mGcm;

    public GcmHelper(Context context) {
        mContext = context;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    public void registerInBackground(String userId) {
        this.mUserId = userId;
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    mGcm = GoogleCloudMessaging.getInstance(mContext);
                    mRegId = mGcm.register(Constants.GCM_SENDER_ID);

                    msg = "Device registered, registration ID=" + mRegId;

                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend(mRegId);

                    // Persist the regID - no need to register again.
                    storeRegistrationId(mRegId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    public static SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
    }

    private void storeRegistrationId(String regId) {
        int appVersion = getAppVersion();
        final SharedPreferences prefs = getGCMPreferences(mContext);
        appVersion = getAppVersion();
        Log.i("GCMHelper", "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.PROPERTY.PROPERTY_REG_ID, regId);

        //Saving appversion just in case if in future required.
        editor.putInt(Constants.PROPERTY.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }


    private int getAppVersion() {
        try {
            PackageInfo packageInfo = mContext.getPackageManager()
                    .getPackageInfo(mContext.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    public String getRegistrationId() {

        final SharedPreferences prefs = getGCMPreferences(mContext);
        String registrationId = prefs.getString(Constants.PROPERTY.PROPERTY_REG_ID, "");
        return registrationId;
    }


    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend(String regId) {
//        String deviceID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);

        Log.e("Registration", "registrationID: " + regId + " GCM_SENDER_ID: " + Constants.GCM_SENDER_ID + " deviceID: " + telephonyManager.getDeviceId());

        // Register User
        NetworkAPIClient.registerDevice(this.mUserId, regId, telephonyManager.getDeviceId(), new ResponseCallback<Notification>() {
            @Override
            public void onSuccess(Notification data) {
                Log.e("Server Registration", "Success");
            }

            @Override
            public void onError(Exception ex, String errorMessage) {
                Log.e("Server Registration", "Failure");
            }
        });
    }
}
