package com.ddstudio.dsc.utils;

/**
 * Created by nhingu on 5/19/15.
 */
public class Constants {

    public static final String USER_ID = "userID";

    public static final String GCM_SENDER_ID ="797573689910"; //1/"860158228741";/2/797573689910

    public static class PARAM {
        public static final String USERNAME = "userName";
        public static final String PASSWORD = "password";
        public static final String PROVIDER_ID = "provider_id";
        public static final String MEMBER__ID = "member_id";
        public static final String MEMBER_ID = "memberId";
        public static final String REG_ID = "regid";
        public static final String CREATED_DATE = "crtd_date";
        public static final String PARAMETER_ID = "parameterId";
        public static final String Days = "days";
        public static final String PARAMERTER_NAME = "parameterName";
        public static final String ANDROID_DEVICE_ID = "Android_Device_Id";
    }

    public static class MOCK_URL {
        public static final String LOGIN = "/api/json/get/V1eZsiCT";
        public static final String PROVIDER_SUMMARY = "/api/json/get/Ey-ehjCT";
        public static final String SUMMARY = "/api/json/get/E1Ko-Nm9";
        public static final String PARAMETER_DETAILS = "/api/json/get/cjoIlnMmdK?indent=2";
        public static final String NOTIFICATION = "/api/json/get/4kiAb4X5";
    }

    public static class URL {
        public static final String LOGIN = "/login";
        public static final String PROVIDER_SUMMARY = "/ProviderSummary";
        public static final String SUMMARY = "/summary";
        public static final String PARAMETER_DETAILS = "/parameterdetails";
        public static final String NOTIFICATION = "/notificationhistory";
        public static final String REGSITER_DEVICE = "/register";
    }

    public static class PROPERTY {
        public static final String PROPERTY_USER_ID = "user_id";
        public static final String PROPERTY_REG_ID = "registration_id";
        public static final String PROPERTY_APP_VERSION = "appVersion";
        public static final String PROPERTY_USER_NAME = "user_name";
        public static final String PROPERTY_USER_TYPE = "user_type";
    }

    public static class DATE_FORMATS {
        public static final String LONG_SERVER_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss.SSSSSS";
        public static final String LONG_NOTIFICATION_DATE_FORMAT = "dd MMMM yyyy,  hh:mm aa";
        public static final String LONG_DISPLAY_DATE_FORMAT = "MMM dd, yyyy kk:mm";
    }
}
