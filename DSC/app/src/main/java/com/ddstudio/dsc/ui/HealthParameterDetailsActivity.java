package com.ddstudio.dsc.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SearchView;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.utils.Constants;
import com.ddstudio.dsc.utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class HealthParameterDetailsActivity extends BaseActivity {

    public static final String TAG = "HealthParameterDetailsActivity";

    @InjectView(R.id.shownotification)
    ImageButton mShownotification;

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    @InjectView(R.id.refresh)
    ImageButton refresh;

    @InjectView(R.id.searchMember)
    SearchView memberSearch;

    @InjectView(R.id.health_parameter_container)
    FrameLayout frameLayout;

    String memberId;
    String parameterID;

    public static void startParameterDetailsActivityWithParameterID(String memberId, String parameterID, String parameterName, Activity activity) {
        Intent i = new Intent(activity, HealthParameterDetailsActivity.class);

        i.putExtra(Constants.PARAM.PARAMETER_ID, parameterID);
        i.putExtra(Constants.PARAM.MEMBER_ID, memberId);
        i.putExtra(Constants.PARAM.PARAMERTER_NAME, parameterName);

        activity.startActivity(i);
    }

    public static void startParameterDetailsActivityWithParameterID(Activity activity) {
        Intent i = new Intent(activity, HealthParameterDetailsActivity.class);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_parameter_details);
        ButterKnife.inject(this);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
            mShownotification.setVisibility(View.GONE);
            this.refresh.setVisibility(View.GONE);

            if (!DSCApplication.DEBUG) {
                String title=getIntent().getExtras().getString(Constants.PARAM.PARAMERTER_NAME);
                getSupportActionBar().setTitle(StringUtils.camelCasingString(title));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        memberSearch.setVisibility(View.GONE);

        if (getIntent() != null
                && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.PARAM.PARAMETER_ID)) {
            parameterID = getIntent().getExtras().getString(Constants.PARAM.PARAMETER_ID);
            memberId = getIntent().getExtras().getString(Constants.PARAM.MEMBER_ID);
        }

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HealthParameterDetailsFragment myFragment = new HealthParameterDetailsFragment();
        if (getIntent() != null
                && getIntent().getExtras() != null
                && getIntent().getExtras().containsKey(Constants.PARAM.PARAMETER_ID)) {
            Bundle bundle = new Bundle();
            bundle.putInt("Days", 7);
            bundle.putString("MemberId", memberId);
            bundle.putString("ParameterId", parameterID);
            myFragment.setArguments(bundle);
        }
        fragmentTransaction.add(R.id.health_parameter_container, myFragment);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
