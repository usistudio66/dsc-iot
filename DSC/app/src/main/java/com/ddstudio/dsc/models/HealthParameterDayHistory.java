package com.ddstudio.dsc.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * To convert date to actual Date object use DateUtils class helper functions.
 * Created by nhingu on 5/27/15.
 */
public class HealthParameterDayHistory {

    @SerializedName("activity_date")
    @Expose
    public String activityDate;

    @SerializedName("kpi_value")
    @Expose
    public float kpiValue;

}
