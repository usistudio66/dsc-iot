package com.ddstudio.dsc.ui.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ddstudio.dsc.ui.HealthParameterDetailsFragment;

/**
 * Created by vassharma on 9/23/2015.
 */
public class HealthTabsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    int weekDays = 7;
    int monthDays = 30;
    String memberId, parameterId;

    public HealthTabsPagerAdapter(FragmentManager fm, int NumOfTabs, String memberId, String parameterId) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.memberId = memberId;
        this.parameterId = parameterId;
    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();
        switch (position) {

            case 0:
                HealthParameterDetailsFragment tabWeek = new HealthParameterDetailsFragment();
                bundle.putInt("Days", weekDays);
                bundle.putString("MemberId", memberId);
                bundle.putString("ParameterId", parameterId);
                tabWeek.setArguments(bundle);
                //return tabWeek;
            case 1:
                HealthParameterDetailsFragment tabMonth = new HealthParameterDetailsFragment();
                bundle.putInt("Days", monthDays);
                bundle.putString("MemberId", memberId);
                bundle.putString("ParameterId", parameterId);
                tabMonth.setArguments(bundle);
                //return tabMonth;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

