package com.ddstudio.dsc.models;

import com.google.gson.annotations.Expose;

/**
 * Created by nhingu on 5/27/15.
 */
public class Notification {

    /*
        dateTime format "yyyy-MM-dd HH:mm:ss z"
     */
    @Expose
    public String message;
    @Expose
    public String timestamp;
    @Expose
    public String value;


}
