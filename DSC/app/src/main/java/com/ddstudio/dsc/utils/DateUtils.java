package com.ddstudio.dsc.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by nhingu on 5/27/15.
 */
public class DateUtils {

    /*
        param date String representation of the date in format yyyy-MM-dd
     */
    public static Date getDateFromString(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(date);
    }

    /*
        param date String representation of the date of format "yyyy-MM-dd HH:mm:ss z" eg."2015-05-18 23:48:40 GMT"
     */
    public static Date getDateFromStringISOFormat(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMATS.LONG_SERVER_DATE_FORMAT);
        return sdf.parse(date);
    }

    /*
            param date String representation of the date in format yyyy-MM-dd
         */
    public static Date getDateFromString(String date, String format) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        return sdf.parse(date);
    }

    public static String getNotificationDisplayDate(String timestamp) {
        String toReturn = "";
        try {
            //2015-06-30 12:41:33//2017-05-16 09:06:20.041746000
            Date date = getDateFromString(timestamp, "yyyy-MM-dd HH:mm:ss zzz");

            /* log the device timezone */
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();

            SimpleDateFormat sdfToDisplay = new SimpleDateFormat(Constants.DATE_FORMATS.LONG_NOTIFICATION_DATE_FORMAT);
            sdfToDisplay.setTimeZone(tz);

            toReturn = sdfToDisplay.format(date);
        } catch (Exception e) {
            e.printStackTrace();


        }
        return toReturn;
    }

    public static String getChartDisplayDate(String timestamp) {
        String toReturn = "";
        try {
            //2015-06-30 12:41:33   //2017-02-12 05:06:07 EST
            Date date = getDateFromString(timestamp, "yyyy-MM-dd HH:mm:ss zzz");

            /* log the device timezone */
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();

            SimpleDateFormat sdfToDisplay = new SimpleDateFormat("dd MMM, yy");
            sdfToDisplay.setTimeZone(tz);

            toReturn = sdfToDisplay.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toReturn;
    }

    public static String getDateValueFromFullDate(String timestamp) {
        String toReturn = "";
        try {
            //2015-06-30 12:41:33
            Date date = getDateFromString(timestamp, "yyyy-MM-dd HH:mm:ss zzz");

            /* log the device timezone */
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();

            SimpleDateFormat sdfToDisplay = new SimpleDateFormat("dd MMM");
            sdfToDisplay.setTimeZone(tz);

            toReturn = sdfToDisplay.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toReturn.replaceAll(" ","\n");
    }

}
