package com.ddstudio.dsc.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nhingu on 9/13/15.
 */
public class ProvidersSummaryData {

    @Expose
    private List<Member> member = new ArrayList<Member>();
    @Expose
    private Boolean success;
    @Expose
    private int status;

    public List<Member> getMember() {
        return member;
    }

    public void setMember(List<Member> member) {
        this.member = member;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
