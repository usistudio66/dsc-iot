package com.ddstudio.dsc.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nhingu on 5/29/15.
 */
public class NotificationHistory {

    @Expose
    public List<Notification> notifications = new ArrayList<Notification>();

}
