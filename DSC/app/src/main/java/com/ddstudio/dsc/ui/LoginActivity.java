package com.ddstudio.dsc.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.gcm.GcmHelper;
import com.ddstudio.dsc.models.User;
import com.ddstudio.dsc.services.NetworkAPIClient;
import com.ddstudio.dsc.services.ResponseCallback;
import com.ddstudio.dsc.ui.view.CustomEditText;
import com.ddstudio.dsc.ui.view.CustomTextView;
import com.ddstudio.dsc.utils.Constants;
import com.ddstudio.dsc.utils.StringUtils;
import com.google.gson.GsonBuilder;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class LoginActivity extends BaseActivity {

    @InjectView(R.id.email)
    CustomEditText mEmailEditText;
    @InjectView(R.id.password)
    CustomEditText mPasswordEditText;
    @InjectView(R.id.email_sign_in_button)
    CustomTextView mEmailSignInButton;
    @InjectView(R.id.sign_upText)
    TextView mSignInText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        mEmailEditText.setText("SCOOPER");
        mPasswordEditText.setText("SCOOPER");

        //member id: DIOTW10000003  passowrd/userid = IFLEMING
        //member id: DIOTW10000002, password/user id = LVINCENT   (for getting parameter details)
        //provider credentails:SCOOPER/SCOOPER  //DIOTW10000001

        mEmailEditText.setSelection(mEmailEditText.getText().length());
        mPasswordEditText.setSelection(mPasswordEditText.getText().length());

        mPasswordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == 0) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mPasswordEditText.getWindowToken(), 0);

                    onSignInButtonClick();
                    return true;
                }
                return false;
            }
        });
        mSignInText.setText(Html.fromHtml(getResources().getString(R.string.sign_up_text)));

        final SharedPreferences prefs = GcmHelper.getGCMPreferences(getApplicationContext());
        String memberId = prefs.getString(Constants.PROPERTY.PROPERTY_USER_ID, "");
        String name = prefs.getString(Constants.PROPERTY.PROPERTY_USER_NAME, "");
        String type = prefs.getString(Constants.PROPERTY.PROPERTY_USER_TYPE, "");

        User u = new User();
        u.name = name;
        u.member_id = memberId;
        u.user_type = type;

        Boolean result1 = (memberId != null) && (memberId.trim().length() > 0);
        Boolean result2 = (name != null) && (name.trim().length() > 0);

        if (result1 && result2) {

            if (u.isProvider()) {
                ProviderMembersActivity.startProviderMembersActivity(LoginActivity.this);
            } else if (u.isMember()) {
                UserSummaryActivity.startUserSummaryActivity(LoginActivity.this, null);
            }
            finish();
        }
    }

    @OnClick(R.id.email_sign_in_button)
    void onSignInButtonClick() {

        showProgressDialog();
        if (DSCApplication.DEBUG) {
            getDataFromSavedJson();
        } else {
            NetworkAPIClient.loginUser(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString(), loginCallBackListener());
        }
    }

    private ResponseCallback<User> loginCallBackListener() {
        return new ResponseCallback<User>() {

            @Override
            public void onSuccess(final User user) {
                hideProgressDialog();
                if (user == null) {
                    Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d("LoginActivity", "Success: " + user.member_id);
                hideProgressDialog();
                onSuccessReturn(user);
            }

            @Override
            public void onError(Exception ex, String errorMessage) {
                hideProgressDialog();
                Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();

            }
        };
    }

    private void getDataFromSavedJson() {
        String notificationJson = StringUtils.loadJSONFromAsset(getBaseContext(), "login.json");
        User user = new GsonBuilder().create().fromJson(notificationJson, User.class);
        onSuccessReturn(user);
    }

    private void onSuccessReturn(User user) {
        final SharedPreferences prefs = GcmHelper.getGCMPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.PROPERTY.PROPERTY_USER_ID, user.member_id);
        editor.putString(Constants.PROPERTY.PROPERTY_USER_NAME, user.name);
        editor.putString(Constants.PROPERTY.PROPERTY_USER_TYPE, user.user_type);
        editor.commit();

        if (user.isProvider()) {
            ProviderMembersActivity.startProviderMembersActivity(LoginActivity.this);
        } else if (user.isMember()) {
            UserSummaryActivity.startUserSummaryActivity(LoginActivity.this, null);
        } else if (user.isClinician()) {
            Toast.makeText(getBaseContext(), "Clinician", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), "None of them", Toast.LENGTH_LONG).show();
        }
        finish();
    }
}



