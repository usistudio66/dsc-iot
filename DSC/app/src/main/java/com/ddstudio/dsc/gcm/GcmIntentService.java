package com.ddstudio.dsc.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.models.User;
import com.ddstudio.dsc.ui.UserSummaryActivity;
import com.ddstudio.dsc.utils.SharedPreferenceHelper;
import com.ddstudio.dsc.utils.StringUtils;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

/**
 * Created by adlotia on 1/28/15.
 */
public class GcmIntentService extends IntentService {

    public static int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    public static final String TAG = "GCM Intent Service";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.

        Log.v("GcmBroadcastReceiver ", extras.getString("message"));
        String messageType = gcm.getMessageType(intent);

        if (!TextUtils.isEmpty(messageType)) {//&& !extras.isEmpty()  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            switch (messageType) {
                case GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR:
                    sendNotification("Send error: " + extras.toString());
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_DELETED:
                    sendNotification("Deleted messages on server: " + extras.toString());
                    // If it's a regular GCM message, do some work.
                    break;
                case GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE:
                    // This loop represents the service doing some work.

                    String message = intent.getExtras().getString("message");
                    // Post notification of received message.
                    sendNotification(message);
                    Log.i(TAG, "Received: " + extras.toString());
                    break;
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {

        if (StringUtils.isBlank(msg)) {
            return;
        }
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        String userId = "";
        String notificationMessage = "";
        String memberName = "";
        try {
            JSONObject job = new JSONObject(msg);
            userId = job.getString("memberid");
            notificationMessage = job.getString("message");
            memberName = job.getString("memberName");
        } catch (Exception e) {
            notificationMessage = msg;
            e.printStackTrace();
        }
        Intent notificationType = null;

        User user = null;
        try {
            user = SharedPreferenceHelper.getUserDetails(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((user != null) && (user.isProvider())) {
            notificationType = new Intent(this, UserSummaryActivity.class);
            notificationType.putExtra("memberid", userId);
            notificationType.putExtra("memberName", memberName);
            notificationType.putExtra("fromnotification", true);
        } else {
            notificationType = new Intent(this, UserSummaryActivity.class);
        }

        notificationType.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        int iUniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
        PendingIntent contentIntent = PendingIntent.getActivity(this, iUniqueId, notificationType, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification))
                        .setContentTitle(getString(R.string.application_name))
                        .setTicker(notificationMessage)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(notificationMessage)).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentText(notificationMessage)
                        .setContentIntent(contentIntent);

        mNotificationManager.notify(iUniqueId, mBuilder.build());
    }
}
