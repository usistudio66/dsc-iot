package com.ddstudio.dsc.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by nhingu on 6/8/15.
 */
public class BaseActivity extends AppCompatActivity {
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showProgressDialog() {
        if(progress == null) {
            progress = new ProgressDialog(this);
            progress.setMessage("Loading...");
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
        }
        progress.show();

    }

    protected void hideProgressDialog() {
        try{
            if(progress != null) {
                progress.dismiss();
            }
        }catch (Exception e){
            //Trying to avoid a crash which may arise if the view is been inflating and the thread requested to hide progress bar..
        }
    }

}
