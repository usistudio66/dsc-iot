package com.ddstudio.dsc.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.models.HealthParameter;
import com.ddstudio.dsc.models.HealthParameters;
import com.ddstudio.dsc.models.Member;
import com.ddstudio.dsc.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nhingu on 9/14/15.
 */
public class MembersListAdapters extends ArrayAdapter<Member> {

    private ArrayList<Member> originalList;
    private ArrayList<Member> filteredList;

    private ListFilter filter;

    public MembersListAdapters(Context context, int resource) {
        super(context, resource);
        originalList = new ArrayList<Member>();
    }

    public MembersListAdapters(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
        originalList = new ArrayList<Member>();
    }

    public MembersListAdapters(Context context, int resource, Member[] objects) {
        super(context, resource, objects);
        originalList = new ArrayList<Member>();
        for (Member member : objects) {
            originalList.add(member);
        }
    }

    public MembersListAdapters(Context context, int resource, int textViewResourceId, Member[] objects) {
        super(context, resource, textViewResourceId, objects);
        originalList = new ArrayList<Member>();
        for (Member member : objects) {
            originalList.add(member);
        }
    }

    public MembersListAdapters(Context context, int resource, List<Member> objects) {
        super(context, resource, objects);
        originalList = new ArrayList<>(objects);
    }

    public MembersListAdapters(Context context, int resource, int textViewResourceId, List<Member> objects) {
        super(context, resource, textViewResourceId, objects);
        originalList = new ArrayList<>(objects);

    }

    public void addAll(List<Member> memebers) {
        try {
            clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        originalList.clear();
        originalList.addAll(memebers);
        super.addAll(memebers);
        notifyDataSetChanged();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.memeber_list_item, parent, false);

        ImageView iv = (ImageView) rowView.findViewById(R.id.member_gender_image);
        TextView tv_membername = (TextView) rowView.findViewById(R.id.member_name);
        LinearLayout llPatientThresholdList = (LinearLayout) rowView.findViewById(R.id.llPatientsThresholdList);

        Member member = getItem(position);

        if (member != null) {
            if (member.isMale()) {
                iv.setImageResource(R.drawable.member_male_icon);
            } else {
                iv.setImageResource(R.drawable.member_female_icon);
            }
        }
        tv_membername.setText(StringUtils.camelCasingString(member.getMemberName()));

        HealthParameters healthParameter = new HealthParameters();
        healthParameter.parameters = member.getParameters();
        for (HealthParameter parm : healthParameter.parameters) {

            double cuurentValue = parm.getCurrentDayValue();
            double thresholdValue = parm.threshold;
            double percentage = (cuurentValue / thresholdValue) * 100;
            if (((percentage >= 85) && (percentage < 100)) || (percentage >= 0 && percentage < 20)) {
                /*do nothing*/
            } else if (percentage >= 100) {
                /*add icon*/

                View view = inflater.inflate(R.layout.threshold_item, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.ivItem);

                if (parm.id.contains(HealthParameters.STEPS_ID)) {
                    imageView.setImageResource(R.drawable.step_icon);
                }
                if (parm.id.contains(HealthParameters.HEART_RATE_ID)) {
                    imageView.setImageResource(R.drawable.heart_icon);
                }
                if (parm.id.contains(HealthParameters.SODIUM_ID)) {
                    imageView.setImageResource(R.drawable.sodium_icon);
                }
                if (parm.id.contains(HealthParameters.CHOLESTROL_ID)) {
                    imageView.setImageResource(R.drawable.choleterol_icon);
                }
                if (parm.id.contains(HealthParameters.CALORIE_ID)) {
                    imageView.setImageResource(R.drawable.calories_icon);
                }
                if (parm.id.contains(HealthParameters.SLEEP_ID)) {
                    imageView.setImageResource(R.drawable.sleep_icon);
                }
                if (parm.id.contains(HealthParameters.CARBOHYDRATE_ID)) {
                    imageView.setImageResource(R.drawable.carbohydrate_icon);
                }
                if (parm.id.contains(HealthParameters.FIBER_ID)) {
                    imageView.setImageResource(R.drawable.fibre_icon);
                }
                imageView.setPadding(7, 0, 15, 0);
                if (imageView.getParent() != null)
                    ((ViewGroup) imageView.getParent()).removeView(imageView);
                llPatientThresholdList.addView(imageView);
            }
        }
        return rowView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new ListFilter();

        return filter;
    }

    private class ListFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 3) {
                ArrayList<Member> filteredItems = new ArrayList<Member>();

                for (int i = 0, l = originalList.size(); i < l; i++) {
                    Member member = originalList.get(i);
                    if (member.getMemberName().toLowerCase().contains(constraint)) {
                        filteredItems.add(member);
                    }
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = originalList;
                    result.count = originalList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            filteredList = (ArrayList<Member>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filteredList.size(); i < l; i++)
                add(filteredList.get(i));
            notifyDataSetInvalidated();
        }
    }
}
