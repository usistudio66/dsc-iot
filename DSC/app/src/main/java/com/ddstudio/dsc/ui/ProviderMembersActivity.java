package com.ddstudio.dsc.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.gcm.GcmHelper;
import com.ddstudio.dsc.models.Member;
import com.ddstudio.dsc.models.ProvidersSummaryData;
import com.ddstudio.dsc.services.NetworkAPIClient;
import com.ddstudio.dsc.services.ResponseCallback;
import com.ddstudio.dsc.ui.adapters.MembersListAdapters;
import com.ddstudio.dsc.utils.Constants;
import com.ddstudio.dsc.utils.SharedPreferenceHelper;
import com.ddstudio.dsc.utils.StringUtils;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ProviderMembersActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    public MembersListAdapters mMemebersListAdapters;
    public String memberId = null;
    @InjectView(R.id.shownotification)
    ImageButton shownotification;
    @InjectView(R.id.refresh)
    ImageButton refresh;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.members_listview)
    ListView membersListview;
    /*@InjectView(R.id.searchView)
    SearchView mSearchView;*/
    @InjectView(R.id.searchMember)
    SearchView memberSearch;
    ArrayList<Member> memberArrayList = new ArrayList<Member>();

    public static void startProviderMembersActivity(Activity activity) {
        Intent intent = new Intent(activity, ProviderMembersActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_memebers);
        ButterKnife.inject(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));

            this.shownotification.setVisibility(View.GONE);
            this.refresh.setVisibility(View.GONE);
        }

        this.memberId = SharedPreferenceHelper.getMemberID(this);
        setupSearchView();
        initializeListView();

        if(DSCApplication.DEBUG){
            getDataFromSavedJson();
        }else{
            fetchMembersData();
        }
    }

    private void initializeListView() {
        membersListview.setTextFilterEnabled(true);
        mMemebersListAdapters = new MembersListAdapters(this, R.layout.memeber_list_item, new ArrayList<Member>());
        membersListview.setAdapter(mMemebersListAdapters);

        membersListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Member member = mMemebersListAdapters.getItem(position);
                Log.d("ProviderMember", "Member Selected: " + member.getMemberName());

                UserSummaryActivity.startUserSummaryActivity(ProviderMembersActivity.this, member);
            }
        });

    }

    private void setupSearchView() {
        // mSearchView.setIconifiedByDefault(false);
        memberSearch.setOnQueryTextListener(this);
        // mSearchView.setSubmitButtonEnabled(true);
        // mSearchView.setQueryHint("Search Here");

        memberSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mMemebersListAdapters.addAll(memberArrayList);
                mMemebersListAdapters.notifyDataSetChanged();
                return false;
            }
        });
    }

    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            membersListview.clearTextFilter();
        } else if (newText.length() >= 3) {
            mMemebersListAdapters.getFilter().filter(newText);
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();

        GcmHelper gcm = new GcmHelper(getApplicationContext());
        if (gcm.getRegistrationId() == null || gcm.getRegistrationId().equals("")) {
            gcm.registerInBackground(memberId);
        }
        Log.d("UserSummaryActivity", "APPREgID: " + gcm.getRegistrationId());
        Log.d("UserSummaryActivity", "SenderId: " + Constants.GCM_SENDER_ID);

    }

    public void fetchMembersData() {


        ResponseCallback<ProvidersSummaryData> callback = new ResponseCallback<ProvidersSummaryData>() {
            @Override
            public void onSuccess(ProvidersSummaryData data) {
                hideProgressDialog();
                if (data.getSuccess() == false) {
                    Toast.makeText(getApplicationContext(), "No data available", Toast.LENGTH_SHORT).show();
                }
                onSuccessReturn(data);

            }

            @Override
            public void onError(Exception ex, String errorMessage) {
                hideProgressDialog();
                Toast.makeText(getApplicationContext(), "No data available", Toast.LENGTH_SHORT).show();
                Log.e("UserSummaryActivity", "message: " + errorMessage);
            }
        };

        NetworkAPIClient.providerSummaryData(this.memberId, callback);
        showProgressDialog();
    }

    private void onSuccessReturn(ProvidersSummaryData data){
        memberArrayList.addAll(data.getMember());
        mMemebersListAdapters.addAll(memberArrayList);
    }

    private void getDataFromSavedJson(){
        String providerSummaryJson = StringUtils.loadJSONFromAsset(getBaseContext(), "providerSummary.json");
        ProvidersSummaryData data = new GsonBuilder().create().fromJson(providerSummaryJson, ProvidersSummaryData.class);
        onSuccessReturn(data);
    }

}
