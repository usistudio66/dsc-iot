package com.ddstudio.dsc.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.SubscriptSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ddstudio.dsc.R;
import com.ddstudio.dsc.gcm.GcmHelper;
import com.ddstudio.dsc.models.HealthParameter;
import com.ddstudio.dsc.models.HealthParameters;
import com.ddstudio.dsc.models.Member;
import com.ddstudio.dsc.models.User;
import com.ddstudio.dsc.services.NetworkAPIClient;
import com.ddstudio.dsc.services.ResponseCallback;
import com.ddstudio.dsc.ui.view.CustomTextView;
import com.ddstudio.dsc.utils.Constants;
import com.ddstudio.dsc.utils.SharedPreferenceHelper;
import com.ddstudio.dsc.utils.StringUtils;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Activity that shows user's data summary for today
 *
 * @author adlotia
 */
public class UserSummaryActivity extends BaseActivity {

    @InjectView(R.id.toolbar)
    Toolbar mSummaryToolbar;

    @InjectView(R.id.shownotification)
    ImageButton mNotificationIcon;

    @InjectView(R.id.summary_today_date)
    CustomTextView mSummaryTodayDate;

    @InjectView(R.id.summary_today_label)
    CustomTextView mSummaryTodayLabel;

    @InjectView(R.id.summary_sleep_value)
    CustomTextView mSummarySleepValue;

    @InjectView(R.id.summary_sleep_threshold)
    CustomTextView mSummarySleepThreshold;

    @InjectView(R.id.summary_steps_value)
    CustomTextView mSummaryStepValue;

    @InjectView(R.id.summary_steps_threshold)
    CustomTextView mSummaryStepThreshold;


    @InjectView(R.id.text_steps)
    CustomTextView mTextSteps;

    @InjectView(R.id.text_hours_sleep)
    CustomTextView mTextHoursSleep;

    @InjectView(R.id.text_calories)
    CustomTextView mTextCalories;

    @InjectView(R.id.summary_heart_rate_value)
    CustomTextView mSummaryHeartRateValue;

    @InjectView(R.id.summary_heart_rate_unit)
    CustomTextView mSummaryHeartRateUnit;

    @InjectView(R.id.summary_cholestrol_value)
    CustomTextView mSummaryCholestrolValue;

    @InjectView(R.id.summary_cholestrol_unit)
    CustomTextView mSummaryCholestrolUnit;

    @InjectView(R.id.summary_calories_value)
    CustomTextView mSummaryCaloriesValue;

    @InjectView(R.id.summary_calories_threshold)
    CustomTextView mSummaryCaloriesThreshold;

    @InjectView(R.id.summary_sodium_intake_value)
    CustomTextView mSummarySodiumIntakeValue;

    @InjectView(R.id.summary_sodium_intake_unit)
    CustomTextView mSummarySodiumIntakeUnit;

    @InjectView(R.id.summary_carbohydrate_value)
    CustomTextView mSummaryCarbohydrateValue;

    @InjectView(R.id.summary_carbohydrate_unit)
    CustomTextView mSummaryCarbohydrateUnit;

    @InjectView(R.id.summary_fibre_intake_value)
    CustomTextView mSummaryFibreIntakeValue;

    @InjectView(R.id.summary_fibre_intake_unit)
    CustomTextView mSummaryFibreIntakeUnit;

    @InjectView(R.id.cholestrol_container)
    RelativeLayout mCholestrolContainer;

    @InjectView(R.id.calories_container)
    FrameLayout mCaloriesContainer;

    @InjectView(R.id.sleep_container)
    FrameLayout mSleepContainer;

    @InjectView(R.id.steps_container)
    RelativeLayout mStepsContainer;

    @InjectView(R.id.sodium_container)
    RelativeLayout mSodiumContainer;

    @InjectView(R.id.carbohydrate_container)
    RelativeLayout mCarbohydrateContainer;

    @InjectView(R.id.fiber_container)
    RelativeLayout mFiberContainer;

    @InjectView(R.id.heart_rate_container)
    RelativeLayout mHeartRateContainer;

    @InjectView(R.id.summary_username)
    TextView mSummaryUserName;

    @InjectView(R.id.steps_progress)
    ProgressBar mStepsProgressBar;

    @InjectView(R.id.sleep_progress)
    ProgressBar mSleepProgressBar;

    @InjectView(R.id.calories_progress)
    ProgressBar mCaloriesProgressBar;

    @InjectView(R.id.searchMember)
    SearchView searchMember;

    private User mUser;

    private HealthParameters mHealthParameters;
    private Member mSelectedMember;

    private boolean isFromNotification = false;

    public static void startUserSummaryActivity(Activity activity, Member member) {
        Intent intent = new Intent(activity, UserSummaryActivity.class);

        if (member != null) {
            intent.putExtra("member", member);
        }

        activity.startActivity(intent);
    }

    private void getMemberDetails() {
        mUser = SharedPreferenceHelper.getUserDetails(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        ButterKnife.inject(this);
        isFromNotification = false;
        getMemberDetails();

        if (mSummaryToolbar != null) {
            setSupportActionBar(mSummaryToolbar);
            mSummaryToolbar.setTitleTextColor(getResources().getColor(R.color.white));

            if (mUser.isMember()) {
                mNotificationIcon.setVisibility(View.VISIBLE);
            } else {
                mNotificationIcon.setVisibility(View.GONE);
            }

            mSummaryToolbar.findViewById(R.id.shownotification).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NotificationActivity.showNotificationHistory(UserSummaryActivity.this, mUser.member_id);
                }
            });

            mSummaryToolbar.findViewById(R.id.refresh).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fetchSummaryData(true);
                }
            });
        }
        searchMember.setVisibility(View.GONE);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        getMemeberData();
        fetchSummaryData(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    private void getMemeberData() {
        if (getIntent() != null) {
            Intent intent = getIntent();
            mSelectedMember = (Member) intent.getSerializableExtra("member");

            isFromNotification = intent.getBooleanExtra("fromnotification", false);
            if (isFromNotification) {
                mSelectedMember = null;
            }

            if (mSelectedMember == null && mUser.isProvider()) {

                mSelectedMember = new Member();

                String memId = intent.getStringExtra("memberid");
                String name = intent.getStringExtra("memberName");

                mSelectedMember.setMemberId(memId);
                mSelectedMember.setMemberName(name);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);

            }

        }
    }

    private void fetchSummaryData(boolean isForce) {
        String memID = mUser.member_id;

        if (mUser.name != null && (mUser.name.trim().length() > 0)) {
            mSummaryUserName.setText(StringUtils.camelCasingString(mUser.name));
        } else {
            mSummaryUserName.setText("");
        }

        if (isForce == false && mSelectedMember != null && mSelectedMember.hasParameters()) {
            mHealthParameters = new HealthParameters();
            mHealthParameters.parameters = mSelectedMember.getParameters();

            mSummaryUserName.setText(StringUtils.camelCasingString(mSelectedMember.getMemberName()));

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            mSummaryUserName.setText(StringUtils.camelCasingString(mSelectedMember.getMemberName()));
            updateUIWithData();
            return;
        } else if (mSelectedMember != null) {
            memID = mSelectedMember.getMemberId();
            mSummaryUserName.setText(StringUtils.camelCasingString(mSelectedMember.getMemberName()));
        }

        showProgressDialog();
        NetworkAPIClient.userSummaryData(memID, new ResponseCallback<HealthParameters>() {
            @Override
            public void onSuccess(HealthParameters data) {
                hideProgressDialog();
                if ((data != null) && (data.parameters != null) && (data.parameters.size() > 0)) {
                    onSuccessReturn(data);
                } else {
                    Toast.makeText(getApplicationContext(), "No data available", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Exception ex, String errorMessage) {
                hideProgressDialog();
                Toast.makeText(getApplicationContext(), "No data available", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadDataFromJson() {
        String memberSummaryJson = StringUtils.loadJSONFromAsset(getBaseContext(), "memberSummary.json");
        HealthParameters user = new GsonBuilder().create().fromJson(memberSummaryJson, HealthParameters.class);
        onSuccessReturn(user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void onSuccessReturn(HealthParameters healthParameters) {
        mHealthParameters = healthParameters;
        updateUIWithData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mUser.isMember()) {
            GcmHelper gcm = new GcmHelper(getApplicationContext());
            if (gcm.getRegistrationId() == null || gcm.getRegistrationId().equals("")) {
                gcm.registerInBackground(mUser.member_id);
            }
            Log.d("UserSummaryActivity", "APPREgID: " + gcm.getRegistrationId());
            Log.d("UserSummaryActivity", "SenderId: " + Constants.GCM_SENDER_ID);
        }
        mSelectedMember = null;

        if (DSCApplication.DEBUG) {
            loadDataFromJson();
        } else {
            getMemeberData();
            fetchSummaryData(false);
        }
    }

    private void updateUIWithData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM, yyyy");
        mSummaryTodayDate.setText(simpleDateFormat.format(new Date(System.currentTimeMillis())));

        for (HealthParameter parm : mHealthParameters.parameters) {
            setupUIForParameter(parm);
        }
    }

    public void onViewClick(View view) {
        HealthParameter parameter = (HealthParameter) view.getTag();

        if (parameter != null
                && !StringUtils.isBlank(parameter.name)
                && !StringUtils.isBlank(parameter.unit)) {
            Log.d("SummaryActivity", "Name: " + parameter.name);
            String memId = mUser.member_id;

            if (mSelectedMember != null) {
                memId = mSelectedMember.getMemberId();
            }

            if (DSCApplication.DEBUG) {
                HealthParameterDetailsActivity.startParameterDetailsActivityWithParameterID(this);
            } else {
                HealthParameterDetailsActivity.startParameterDetailsActivityWithParameterID(memId, parameter.id, parameter.name, this);
            }
        } else {
            Toast.makeText(getBaseContext(), "No data available!", Toast.LENGTH_LONG).show();
        }
    }


    private void setupUIForParameter(HealthParameter parameter) {

        if (parameter == null) {
            return;
        }

        double percentage = (parameter.currentDayValue / parameter.threshold) * 100;

        switch (parameter.id) {
            case HealthParameters.STEPS_ID:
                mStepsContainer.setTag(parameter);
                mSummaryStepValue.setText(String.valueOf(parameter.getCurrentDayValue()));
                mSummaryStepThreshold.setText("of " + parameter.threshold);
                setupProgressView(mStepsProgressBar, percentage, mTextSteps);

                //mSummarySteps.setText(getSpannableString(String.valueOf(parameter.getCurrentDayValue()),
                //       R.color.white_four, String.valueOf(parameter.threshold), R.color.white_three,
                //       null, R.color.values_subscript)

                break;
            case HealthParameters.SLEEP_ID:
                mSleepContainer.setTag(parameter);
                mSummarySleepValue.setText(String.valueOf(parameter.getCurrentDayValue()));
                mSummarySleepThreshold.setText("of " + parameter.threshold);
                setupProgressView(mSleepProgressBar, percentage, mTextHoursSleep);

                //mSummarySleep.setText(getSpannableString(String.valueOf(parameter.getCurrentDayValue()),
                //        R.color.white_four, "\n" + parameter.threshold, R.color.white_three,
                //        null, R.color.values_subscript));
                break;
            case HealthParameters.CALORIE_ID:
                mCaloriesContainer.setTag(parameter);
                mSummaryCaloriesValue.setText(String.valueOf(parameter.getCurrentDayValue()));
                mSummaryCaloriesThreshold.setText("of " + parameter.threshold);
                setupProgressView(mCaloriesProgressBar, percentage, mTextCalories);

                //mSummaryCaloriesIntake.setText(getSpannableString(String.valueOf(parameter.getCurrentDayValue()),
                //        R.color.white_four, String.valueOf(parameter.threshold), R.color.white_three, null,
                //        R.color.values_subscript));
                break;
            case HealthParameters.FIBER_ID:
                mFiberContainer.setTag(parameter);
                setTextView(mSummaryFibreIntakeValue, parameter);
                setUnitDate(mSummaryFibreIntakeUnit, parameter.unit);
                break;
            case HealthParameters.CARBOHYDRATE_ID:
                mCarbohydrateContainer.setTag(parameter);
                setTextView(mSummaryCarbohydrateValue, parameter);
                setUnitDate(mSummaryCarbohydrateUnit, parameter.unit);
                break;
            case HealthParameters.HEART_RATE_ID:

                mHeartRateContainer.setTag(parameter);
                setTextView(mSummaryHeartRateValue, parameter);
                setUnitDate(mSummaryHeartRateUnit, "bpms");

                // mSummaryHeartRate.setText(getSpannableString(String.valueOf(parameter.getCurrentDayMinValue())
                // + " ~ " + String.valueOf(parameter.getCurrentDayMAxValue()),
                // getColorForValueAgainstThreshold(parameter.getCurrentDayMAxValue(), parameter.threshold),
                //         null, -1, "bpms", R.color.values_subscript));
                break;
            case HealthParameters.CHOLESTROL_ID:
                mCholestrolContainer.setTag(parameter);
                setTextView(mSummaryCholestrolValue, parameter);
                setUnitDate(mSummaryCholestrolUnit, parameter.unit);
                break;
            case HealthParameters.SODIUM_ID:
                mSodiumContainer.setTag(parameter);
                setTextView(mSummarySodiumIntakeValue, parameter);
                setUnitDate(mSummarySodiumIntakeUnit, parameter.unit);
                break;
            default:
                break;
        }
    }

    private void setUnitDate(TextView textView, String unit) {
        textView.setText(unit);
        textView.setTextColor(getResources().getColor(R.color.white_two));
    }


    private void setupProgressView(ProgressBar progressBar, double percentage, TextView textView) {
        progressBar.setProgress((int) percentage);

        //if (percentage > 100) {
        //    progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular_progress_drawable_foreground_red));
        //} else {
        progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.circular_progress_drawable_foreground));
        //}

        if (percentage >= 100) {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_icon, 0, 0, 0);
            textView.setCompoundDrawablePadding(5);
            textView.setTextColor(getResources().getColor(R.color.lipstick_color));
        }
    }

    public int getColorForValueAgainstThreshold(HealthParameter parameter) {

        double percentage = (parameter.currentDayValue / parameter.threshold) * 100;
        if (((percentage >= 85) && (percentage < 100)) || (percentage >= 0 && percentage < 20)) {
            return R.color.dusty_orange;
        } else if (percentage >= 100) {
            return R.color.lipstick_color;
        }

        return R.color.white_two;
    }

    public int getColorForValueAgainstThreshold(double value, double threshold) {//white, orange, red

        double percentage = (value / threshold) * 100;
        if (((percentage >= 85) && (percentage <= 100)) || (percentage >= 0 && percentage < 20)) {
            return R.color.dusty_orange;
        } else if (percentage > 100) {
            return R.color.lipstick_color;
        }

        return R.color.white_two;
    }

    /*
    if colorCodeForSuperScript is -1 then it will apply default color set in the xml.
     */
    private SpannableString getSpannableString(String mainText, int colorCodeForSuperScript, String thresholdValue, int thresholdColorCodeForSuperScript, String subScriptText, int colorCodeForSubscript) {
        String text = mainText;
        int subscriptStartPosition = mainText.length();

        if (thresholdValue != null) {
            text += "/" + thresholdValue;
            subscriptStartPosition = text.length();
        }

        if (null != subScriptText) {
            text += " " + subScriptText;
        }

        SpannableString ss1 = new SpannableString(text);

        if (colorCodeForSuperScript != -1) {
            ss1.setSpan(new ForegroundColorSpan(getResources().getColor(colorCodeForSuperScript)), 0, mainText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }

        // ss1.setSpan(new AbsoluteSizeSpan(23, true), 0, subscriptStartPosition, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        if (thresholdValue != null && subScriptText != null) {
            ss1.setSpan(new ForegroundColorSpan(getResources().getColor(thresholdColorCodeForSuperScript)), mainText.length() + 1, subscriptStartPosition, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        int totalTextLength = (text.length());
        ss1.setSpan(new AbsoluteSizeSpan(12, true), subscriptStartPosition, totalTextLength, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        ss1.setSpan(new SubscriptSpan(), subscriptStartPosition, totalTextLength, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        ss1.setSpan(new ForegroundColorSpan(getResources().getColor(colorCodeForSubscript)), subscriptStartPosition, totalTextLength, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        return ss1;
    }

    private SpannableString getSpannableString(String mainText, int colorCodeForSuperScript, String thresholdValue, int thresholdColorCodeForSuperScript) {
        String text = mainText;
        int subscriptStartPosition = mainText.length();
        if (thresholdValue != null) {
            text += "/" + thresholdValue;
            subscriptStartPosition = text.length();
        }

        SpannableString ss1 = new SpannableString(text);

        if (colorCodeForSuperScript != -1) {
            ss1.setSpan(new ForegroundColorSpan(getResources().getColor(colorCodeForSuperScript)), 0, mainText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }

        if (thresholdValue != null) {
            ss1.setSpan(new ForegroundColorSpan(getResources().getColor(thresholdColorCodeForSuperScript)), mainText.length(), subscriptStartPosition, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        int totalTextLength = (text.length());
        ss1.setSpan(new AbsoluteSizeSpan(12, true), subscriptStartPosition, totalTextLength, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        ss1.setSpan(new SubscriptSpan(), subscriptStartPosition, totalTextLength, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        return ss1;
    }

    private void setTextView(TextView textView, HealthParameter parameter) {
        double percentage = (parameter.currentDayValue / parameter.threshold) * 100;

        if (percentage >= 100) {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_icon, 0, 0, 0);
            textView.setCompoundDrawablePadding(5);
        }
        textView.setText(
                getSpannableString(
                        "" + parameter.getCurrentDayValue(),
                        getColorForValueAgainstThreshold(parameter.currentDayValue, parameter.threshold),
                        "" + parameter.threshold,
                        R.color.white_two));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isFromNotification && mUser.isProvider()) {
            Intent i = new Intent(this, ProviderMembersActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

}
